/***********************************************************************************
 * 
 *          Copyright 2018 - 
 *              Bart Brinks && Maarten Rengelink
 *              
 * 
 *          READ ME:
 *          targetClassname is de naam van de class die je ge-highlight wil hebben
 *          
 *          Maak een class aan in je css, met de css stijl veranderingen
 *          die gewenst zijn en geef die aan in classActive
 * 
 *          De knoppen (links) die gehighlight moeten worden een id meegeven, in je html 
 *          met a-(id van element) aanduiden.
 *          
 *          Kijk in de HTML en CSS hoe je dit toepast
 *          Voor vragen: kom maar langs!
 ************************************************************************************/
var targetClassname = "";
var classActive = "";

var isInViewport = function(elem) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};

function onScroll() {
    var elements = document.getElementsByClassName(targetClassname);
    for (let i = 0; i < elements.length; i++) {
        let element = elements[i];
        var targetElement = document.getElementById("a-" + element.id);
        if (targetElement != null) {
            if (isInViewport(element)) {
                targetElement.classList.add(classActive);
            } else {
                targetElement.classList.remove(classActive);
            }
        }
    }
}

document.addEventListener("scroll", function() {
    onScroll();
});